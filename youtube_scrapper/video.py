import bs4
import dataclasses
import datetime
import re
import requests
import typing

from youtube_scrapper.utils import json_find_recursive
from youtube_scrapper.utils import find_values_by_key


@dataclasses.dataclass
class Video:
    """ The class represents youtube video, characterized by video ID

        Attributes
        ----------
        id : str
            The video ID.
        title : str
            Title of the video.
        author : str
            Author of the video.
        description : str
            Description of the video.
        thumbnail : str
            URL to the thumbnail of the video.
        published_at : datetime.datetime
            The date and time when the video was created.
        channel_id : str
            ID of channel on which the video was uploaded.
        duration : int
            Duration of the video (seconds).
        view_count : int
            The number of times the channel has been viewed.
        like_count : int
            The number of likes.
        comment_count : int
            The number of comments for the video.
        """

    id: str = dataclasses.field()
    title: str = dataclasses.field(init=False)
    author: str = dataclasses.field(init=False)
    description: str = dataclasses.field(init=False)
    keywords: typing.List[str] = dataclasses.field(init=False)
    thumbnail: str = dataclasses.field(init=False)
    published_at: datetime.datetime = dataclasses.field(init=False)
    channel_id: str = dataclasses.field(init=False)
    duration: int = dataclasses.field(init=False)
    view_count: int = dataclasses.field(init=False)
    like_count: typing.Union[int, None] = dataclasses.field(init=False)
    # comment_count: int = dataclasses.field(init=False)

    def __info(self):
        """The method returns all information about video in json format"""
        pass

    def get_metadata(self) -> None:
        url = f"https://www.youtube.com/watch?v={self.id}"
        session = requests.session()
        session.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
        session.headers["Accept-Language"] = "en-US,en;q=0.5"
        session.cookies.set("CONSENT", "YES+cb", domain=".youtube.com")
        response = session.get(url)
        soup = bs4.BeautifulSoup(response.text, features="html.parser")

        yt_initial_player_response = soup.findAll(text=re.compile("var ytInitialPlayerResponse"))
        video_details = next(find_values_by_key('"videoDetails":', str(yt_initial_player_response)))
        self.title = video_details["title"]
        self.author = video_details["author"]
        self.description = video_details.get("shortDescription", "")
        self.keywords = video_details.get("keywords", [])
        self.thumbnail = video_details["thumbnail"]["thumbnails"][-1]["url"]
        self.channel_id = video_details["channelId"]
        self.duration = int(video_details["lengthSeconds"])
        self.view_count = int(video_details["viewCount"])
        # self.comment_count = None

        yt_initial_data = soup.findAll(text=re.compile("var ytInitialData"))
        top_level_buttons = find_values_by_key('"topLevelButtons":', str(yt_initial_data))
        for button in next(top_level_buttons):
            try:
                if button["toggleButtonRenderer"]["defaultIcon"]["iconType"] == "LIKE":
                    raw_like_count = button["toggleButtonRenderer"]["defaultText"]["accessibility"]["accessibilityData"]["label"]
                    self.like_count = int(raw_like_count.replace(",", "").replace(" likes", ""))
                    break
            except KeyError:
                self.like_count = None
                pass

        video_primary_info_renderer = next(find_values_by_key('"videoPrimaryInfoRenderer":', str(yt_initial_data)))
        date = video_primary_info_renderer["dateText"]["simpleText"].replace("Streamed live on ", "").replace("Premiered ", "")
        self.published_at = datetime.datetime.strptime(date, "%b %d, %Y")


    def __scrap_comments(self):
        """The method scrapes comments from video
        IN DEVELOPMENT

        """
        pass


def test():
    v = Video("0B83Jk1qB9c")
    v.get_metadata()
    print(dataclasses.asdict(v))



if __name__ == "__main__":
    test()
