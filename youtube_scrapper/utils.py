import datetime
import json
import re


class DateTimeEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime.datetime):
            return (str(z.date()))
        else:
            return super().default(z)


def find_values_by_key(key, string):
    brackets = {
        ")": "(",
        "]": "[",
        "}": "{"
    }
    quotations = ['"']
    stack = ["start"]
    string_length = len(string)
    for matching in re.finditer(key, string):
        result = {}
        result["begin"] = matching.span()[1]
        position = matching.span()[1]
        while position < string_length:
            if stack[-1] in quotations:
                if string[position] in quotations and string[position - 1] != "\\":
                    stack.pop(-1)
                else:
                    pass
            elif string[position] in brackets.values() or (string[position] in quotations and string[position - 1] != "\\"):
                stack.append(string[position])
            elif string[position] in brackets.keys():
                if stack.pop(-1) != brackets[string[position]]:
                    raise Exception("Unbalanced brackets")
                if stack == ["start"]:
                    result["end"] = position + 1
                    break
            position += 1
        raw = string[result["begin"]:result["end"]].replace('\\xa', ' ').replace('\\\\"', '\\"')
        # FIXME probably we need to add here some kind of warning?
        while True:
            try:
                yield json.loads(raw)
                break
            except json.decoder.JSONDecodeError as error:
                character = int(re.findall(re.compile(r"Invalid \\escape: line 1 column (\d*)"), str(error))[0])
                raw = raw[:character-1] + raw[character:]


def json_find_recursive(key_to_search, json_data):
    found = {}
    if isinstance(json_data, dict):
        if key_to_search in json_data.keys():
            found[key_to_search] = json_data[key_to_search]
        elif len(json_data.keys()) > 0:
            for key in json_data.keys():
                result = json_find_recursive(key_to_search, json_data[key])
                if result:
                    for subkey, value in result.items():
                        found[subkey] = value
    elif isinstance(json_data, list):
        for node in json_data:
            result = json_find_recursive(key_to_search, node)
            if result:
                for key, value in result.items():
                    found[key] = value
    return found
