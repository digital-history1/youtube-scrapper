import bs4
import dataclasses
import datetime
import json
import re
import requests
import typing

from youtube_scrapper.utils import DateTimeEncoder
from youtube_scrapper.utils import json_find_recursive
from youtube_scrapper.utils import find_values_by_key
from youtube_scrapper.video import Video


API_ENDPOINT = "https://www.youtube.com/youtubei/v1/browse"


class AmbiguousField(Exception):
    pass


@dataclasses.dataclass
class Channel:
    """ The class represents youtube channel, characterized by channel ID

    Attributes
    ----------
    id : str
        The channel ID.
    title : str
        Title of the channel.
    description : str
        Description of the channel.
    published_at : datetime.datetime
        The date and time when the channel was created.
    view_count : int
        The number of times the channel has been viewed.
    comment_count : int
        The number of comments for the channel.
    subscriber_count : int
        The number of subscribers that the channel has.
    video_count : int
        The number of videos uploaded to the channel.
    videos_ids : list
        List of ID of related videos.
    """

    id: str
    title: str = dataclasses.field(init=False)
    description: str = dataclasses.field(init=False)
    published_at: datetime.datetime = dataclasses.field(init=False)
    view_count: int = dataclasses.field(init=False)
    comment_count: int = dataclasses.field(init=False)
    subscriber_count: int = dataclasses.field(init=False)
    video_count: int = dataclasses.field(init=False)
    videos_ids: typing.List[str] = dataclasses.field(default_factory=list)

    @classmethod
    def get_by_title(cls, channel_title):
        url = f"https://www.youtube.com/c/{channel_title}"
        session = requests.session()
        session.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
        session.cookies.set("CONSENT", "YES+cb", domain=".youtube.com")
        response = session.get(url)
        soup = bs4.BeautifulSoup(response.text, features="html.parser")
        matches_initial_data = soup.findAll(text=re.compile("externalId"))
        channel_metadata_renderer = find_values_by_key('"channelMetadataRenderer":', str(matches_initial_data))
        channel_id = next(channel_metadata_renderer)["externalId"]
        return cls(channel_id)

    def __info(self):
        """The method returns all information about channel in json format"""
        pass

    def __playlist(self):
        """The method returns url adress to json containing all videos from channel"""
        pass

    def scrap_videos_metadata(self):
        """The method scrapes all videos from channel
        Return
        ------
        videos : list
            List of video objects
        """
        self.__videos_list()
        print(self.video_count)
        results = []
        for i, video_id in enumerate(self.videos_ids):
            print(f"Scrapping video: {i + 1}/{self.video_count}")
            video = Video(video_id)
            video.get_metadata()
            results.append(video)
        return results

    def __videos_list(self):
        """The method returns list of all videos IDs
        """
        url = f"https://www.youtube.com/channel/{self.id}/videos?view=0&sort=cc&flow=grid"
        session = requests.session()
        session.headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
        session.cookies.set("CONSENT", "YES+cb", domain=".youtube.com")
        response = session.get(url)
        soup = bs4.BeautifulSoup(response.text, features="html.parser")

        # matches_initial_data = soup.findAll(text=re.compile("ytInitialData"))
        matches_initial_data = soup.findAll(text=re.compile("gridVideoRenderer"))
        if len(matches_initial_data) > 1:
            raise AmbiguousField("More than 1 matches for ytInitialData")
        else:
            videos_grid = find_values_by_key('"gridVideoRenderer":', str(matches_initial_data))

        for video in videos_grid:
            self.videos_ids.append(video["videoId"])

        continuation_parameters = next(find_values_by_key('"continuationEndpoint":', str(matches_initial_data)))

        matches_innertube_context = soup.findAll(text=re.compile("INNERTUBE_CONTEXT"))
        if len(matches_innertube_context) > 1:
            raise AmbiguousField("More than 1 matches for ytInitialData")
        else:
            web_player_context_config = next(find_values_by_key('"WEB_PLAYER_CONTEXT_CONFIG_ID_KEVLAR_SHORTS":', str(matches_innertube_context)))
            innertube_context = next(find_values_by_key('"INNERTUBE_CONTEXT":', str(matches_innertube_context)))

        innertube_api_key = web_player_context_config["innertubeApiKey"]
        client = innertube_context["client"]

        ajax_data = {
            "context": {
                "clickTracking": {
                    "clickTrackingParams": continuation_parameters["clickTrackingParams"]
                },
                "client": client
            },
            "continuation": continuation_parameters["continuationCommand"]["token"]
        }

        self.__get_ajax_videos(session, innertube_api_key, ajax_data)

    def __get_ajax_videos(self, session, innertube_api_key, ajax_data):
        """The method gets next videos using AJAX

        Args:
            session: current requests session
            innertube_api_key: innertubeApiKey obtainer from current session
            ajax_data: AJAX data including token and stuff to get next videos
        """
        params = {"key": innertube_api_key}
        response = session.post(API_ENDPOINT, params=params, json=ajax_data)
        assert response.ok
        continuation_items = json_find_recursive("continuationItems", response.json())["continuationItems"]
        for item in continuation_items:
            try:
                self.videos_ids.append(item["gridVideoRenderer"]["videoId"])
            except KeyError:
                continuation_item_renderer = item["continuationItemRenderer"]
        try:
            new_ajax_data = {
                "context": {
                    "clickTracking": {
                        "clickTrackingParams": continuation_item_renderer["continuationEndpoint"]["clickTrackingParams"]
                    },
                    "client": ajax_data["context"]["client"]
                },
                "continuation": continuation_item_renderer["continuationEndpoint"]["continuationCommand"]["token"]
            }
            self.__get_ajax_videos(session, innertube_api_key, new_ajax_data)
        except UnboundLocalError:
            self.__count_videos()
            return

    def __count_videos(self):
        """The method counts videos after their IDs are obtrained
        """
        self.video_count = len(self.videos_ids)

    def dump_all_videos_to_json(self, file_name):
        videos = self.scrap_videos_metadata()
        with open(file_name, 'w') as file:
            json.dump([dataclasses.asdict(video) for video in videos], file, cls=DateTimeEncoder)


def test():
    c = Channel.get_by_title("NetworkChuck")
    # meta = c.scrap_videos_metadata()
    c.dump_all_videos_to_json("videos.json")


if __name__ == "__main__":
    test()
