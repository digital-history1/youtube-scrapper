# youtube-scrapper

Tool for scrapping Youtube content

## Requirements

`python==3.8.13`
`beautifulsoup4==4.11.1`
`requests==2.27.1`

## Installation

```bash
pip install git+https://gitlab.com/digital-history1/youtube-scrapper.git
```

## Usage

Scrapping all videos metadata from given channel and saving to JSON file:

```python
import youtube_scrapper
c = youtube_scrapper.Channel.get_by_title("Channel_Name")
c.dump_all_videos_to_json("videos.json")
```

Scrapping single video metadata using its ID:

```python
import youtube_scrapper
v = youtube_scrapper.Video("VIDEO_ID")
print(v.get_metadata())
```

## License

[MIT](LICENSE.md) Copyright (c) 2022 Digital History
