# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* Backward compatibility with Python 3.8. (#16)

### Fixed

* Error with field `published_at` when video was streamed or published. (#15)

## 0.1.0 - 22.05.2022

### Added

* Method for dumping all videos metadata to JSON. (#9)
* Scrap video basic metadata. (#7)
* Get channel ID from its title. (#6)
* Utilities for searching in JSONs and JSON-like strings. (#5)
* Scrapping videos IDs. (#5)
* Cleanup classes `Channel` and `Video`. (#4)
* Classes `Channel` and `Video` from SMSaA. (#3)
* Clean up README. (#2)
* MIT license. (#2)
* Project inital structure. (#2)

### Changed

* `README.md` contains minimal information about package. (#10)

### Fixed

* Broken imports. (#11)
* Not listed Dependencies. (#8)

### Removed

* `environment.yml` file. (#8)

[Unreleased]: https://gitlab.com/digital-history1/youtube-scrapper/compare/0.1.0...master
