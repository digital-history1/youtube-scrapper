from setuptools import setup, find_packages


setup(
    name="youtube_scrapper",
    packages=["youtube_scrapper"] + find_packages(),
    version="0.1.0",
    description="Package for scraping data from YouTube.",
    author="Cyprian Kleist",
    author_email="Cyp2505@wp.pl",
    license="MIT",
    install_requires=[
        "beautifulsoup4==4.11.1",
        "requests==2.27.1"
    ]
)
